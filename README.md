# TZ Bot
TZ is a Discord bot that uses role names to indicate the local time of users. It is self-hostable and very easy to use. 

## Installation
### Creating a Discord bot
1. Create a new application on the [Discord Developper Portal](https://discord.com/developers/applications).
2. Click on `Bot` and click on `Add Bot`. Then copy the token for later
3. Click on `OAuth` and select `Bot` then `Manage roles` and `Send messages`
4. Copy the link and go on it to invite the bot to your server

### Installing and running the bot
1. Make sure you have the requirements installed, those are Python3, Pip3 and git
2. Clone this repository

```bash
git clone https://codeberg.org/SnowCode/tz-bot
cd tz-bot
```

3. Install the necessary pip packages

```bash
pip install -r requirements.txt
```

4. Create the configuration file, then edit it to add the bot token in it.

```bash
cp config.sample.py config.py
nano config.py
```

5. Run the bot to see if everything works (close it by pressing CTRL+C)

```bash
python3 bot.py
```

6. (if you are on a linux system) Configure the service file, and start it

```bash
sed -i "s|ABSOLUTE_PATH|$(pwd)|g" tz-bot.service
sed -i "s|USER|$USER|g" tz-bot.service
sudo cp tz-bot.service /etc/systemd/system/
sudo systemctl start tz-bot
```

## Using the bot
The bot is very easy to use simply run:

```bash
>add <city name or timezone name>
```

For instance:

```bash
>add london
```

or 

```bash
>add UTC
```

Then if you want to reset them you can use `>remove`

> **Note**: Don't use country names, the script is meant to work with either cities or exact timezone names, if you use something else, it's going to do some weird shit.

## License
This bot is under [this license](./LICENSE)
