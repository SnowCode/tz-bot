import discord, json
from discord.utils import get
from discord.ext import commands
from config import token, delay
bot = commands.Bot(command_prefix='>')

import datetime, pytz
from asyncio import sleep

# To get tz from location
from geopy.geocoders import Nominatim
from timezonefinder import TimezoneFinder
g = Nominatim(user_agent="timezone.py")
tzf = TimezoneFinder()

# For debugging
import logging
logging.basicConfig(level=logging.INFO)

async def set_timezone(timezone, role_id):
    now = datetime.datetime.now(pytz.timezone(timezone))
    now = f"{now.hour}h - {timezone}"
    role = get(bot.guilds[0].roles, id=role_id)
    await role.edit(name=now)
    print("Timezones fetched")

@bot.event
async def on_ready():
    print("Bot logged in.")

    # Every X seconds, going to rename all roles to correct time
    while True:
        timezones = json.loads(open("timezones.json").read())
        for timezone in timezones:
            role_id = timezones[timezone]
            await set_timezone(timezone, role_id)
        await sleep(delay)

@bot.command()
async def set(ctx, *, location):
    """Simply provide a location name (city is best) or the exact timezone name in 'Europe/Brussels' or 'UTC' format"""
    # Removing all tz roles before adding new ones
    timezones = json.loads(open("timezones.json").read())
    for timezone in timezones:
        print(timezone)
        role = get(ctx.guild.roles, id=timezones[timezone])
        await ctx.message.author.remove_roles(role)

    # Find timezone by exact timezone
    try:
        pytz.timezone(location)
    except:
        # Find timezone by city (NOT COUNTRY)
        try:
            loc = g.geocode(location)
            timezone = tzf.timezone_at(lng=loc.longitude, lat=loc.latitude)
        except:
            await ctx.send("Invalid timezone :x:")
            return None

    # If timezone role already exists, then get it from id otherwise create it (and add it to .json)
    try:
        role_id = timezones[timezone]
        role = get(ctx.guild.roles, id=role_id)
    except:
        role = await ctx.guild.create_role(name=timezone)
        timezones[timezone] = role.id
        open("timezones.json", "w").write(json.dumps(timezones))

    # Add the role to the user and fetch time
    await ctx.message.author.add_roles(role)
    await ctx.send(f"Your timezone has been set to `{timezone}` :clock1:")
    await set_timezone(timezone, timezones[timezone])

bot.run(token)
